<?php
/**
 * @file
 * Context condition plugin for smart_ip.
 */

/**
 * Exposes smart_ip information as context condition.
 */
class context_field_display_reaction extends context_reaction {
  function options_form($context) {
    $options = $this->fetch_from_context($context, 'options');
//    $form = parent::condition_form($context);
//    $form['#type'] = 'fieldset';
//    $form['#tree'] = TRUE;
    $form['field_name'] = array(
      '#title' => t('Field name'),
      '#type' => 'textfield',
      '#descripion' => t('Machine name of the field.'),
      '#default_value' => isset($options['field_name']) ? $options['field_name'] : '',
    );
    $form['bundle'] = array(
      '#title' => t('Bundle'),
      '#type' => 'textfield',
      '#descripion' => t('Machine name of the target bundle (like content type in case of node entities).'),
      '#default_value' => isset($options['bundle']) ? $options['bundle'] : '',
    );
    $form['formatter_name'] = array (
      '#title' => t('Formatter name'),
      '#type' => 'textfield',
      '#descripion' => t('Machine name of the new formatter to be used (Use "hidden" to hide field).'),
      '#default_value' => isset($options['formatter_name']) ? $options['formatter_name'] : '',
    );
    $form['display_module'] = array (
      '#title' => t('Display module'),
      '#type' => 'textfield',
      '#descripion' => t('Machine name of the new module used to display the field (Optional).'),
      '#default_value' => isset($options['display_module']) ? $options['display_module'] : '',
    );
    return $form;
  }
  function execute($field_name, $bundle, &$display) {
    $contexts = $this->get_contexts();
    foreach ($contexts as $context) {
      $options = $this->fetch_from_context($context);
      if ($options['field_name'] == $field_name && $options['bundle'] == $bundle) {
        if (!empty($options['formatter_name'])) {
          $display['type'] = $options['formatter_name'];
        }
        if (!empty($options['display_module'])) {
          $display['module'] = $options['display_module'];
        }
      }
    }
  }
}
